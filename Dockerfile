FROM rocker/shiny:4.2.2
RUN apt-get update
RUN apt-get install -y libssl-dev libxml2-dev \
    && rm -rf /var/lib/apt/lists/*

RUN R -e "install.packages(c('shiny', 'tidyverse', 'shinyjqui', 'shinyjs', 'shinyWidgets', 'shinythemes', 'pheatmap', 'UpSetR', 'ggExtra', 'DT', 'purrr', 'openxlsx'))"
#RUN R -e "install.packages(c('dplyr'))"
#RUN R -e "remove.packages("tidyverse")"
#RUN R -e "install.packages(tidyverse)"

COPY /app /srv/shiny-server/

RUN sudo chown -R shiny:shiny /home
USER shiny
CMD ["/usr/bin/shiny-server"]
