renderAnnotationsTable <- function(ANNO_STUDIES) {
    renderDT({
        return(DT::datatable(ANNO_STUDIES,
                             rownames = FALSE,
                             fillContainer = TRUE,
                             selection = "single",
                             filter = "top",
                             extensions = list("FixedColumns" = list(leftColumns = 1,
                                                                     rightColumns = 0),
                                               "Scroller" = NULL),
                             options = list(
                                 #scrollY = 400,
                                 #scroller = TRUE,
                                 #scrollX = 400,
                                 #fixedColumns = TRUE,
                                 autoWidth = TRUE,
                                 pageLength = nrow(ANNO_STUDIES),
                                 columnDefs = list(
                                     list(
                                         class = "dt-body-wrap",
                                         targets = c(4, 5, 6, 7),
                                         width = "450px"
                                     )
                                 )

                             )))
    })
}
