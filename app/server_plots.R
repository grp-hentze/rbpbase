RBDplot <- renderPlot({
    cat(  ( SelectedRBPTable()  %>% .[["knownDomain"]] %>% table ) )
    
    SelectedRBPTable()  %>% 
        mutate(knownDomain = factor(ifelse(knownDomain, "has known RBD", "no known RBD"), levels=c("no known RBD", "has known RBD"))) %>%
        ggplot(aes(x = knownDomain)) +# facet_grid(~location) + 
        geom_bar(position = "stack", width=0.5) +
        xlab("")  +  mytheme  + 
        removeGrid() + theme(legend.title=element_blank()) +
        scale_fill_manual(values = c("grey", cbbPalette[c(1,7,4,5)]))+
        geom_text(stat='count', aes(label=..count..),
                  position=position_stack(vjust = 0.5), size=5.5) +
        ylab("") + 
        theme(plot.title = element_text(hjust = 0.5),
              axis.text.y = element_blank(),
              axis.text.x = element_text(size=13), 
              axis.ticks=element_blank(),
              axis.line=element_blank())#+ coord_cartesian(xlim = c(0.4,0.8))
})


UpsetPlot <- renderPlot({
    x <- SelectedRBPTable()
    # select only selected columns
    selected_studies <- colnames(x)[ colnames(x) %in% all_studies ]
    x <- x[,c("UNIQUE", selected_studies)]
    
    x <- x %>% mutate_at(vars(selected_studies), as.numeric)
    x <- x %>% column_to_rownames("UNIQUE")
    
    upset(data = x, nsets = ncol(x), nintersects = 30, keep.order = F, order.by="freq")
    
})
