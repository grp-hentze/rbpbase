# Dockerfile for RBPBase Shiny app
Shiny app and Docker file for RBPBase - a collection of RNA-binding protein high-throughput detection studies. 


# Local test
```
docker run -p 8888:3838 git.embl.de:4567/schwarzl/rbpbase:v0.1
```

# Login to the registry
```
docker login  git.embl.de:4567
```

# Build the image
Remember to update the tag for every build, so we can keep the log.
```
docker build -t git.embl.de:4567/schwarzl/rbpbase:v0.1 .
```

# Push it to the registry
```
docker push git.embl.de:4567/schwarzl/rbpbase:v0.1
```
